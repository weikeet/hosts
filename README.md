# hosts

主要是通过修改 host 和使用 docker NginxProxyManager 实现反向代理内网中的 IP 和端口，实现 https 安全的域名访问。

## Getting started

配置 hosts 教程

这里推荐使用 SwitchHosts 配置 hosts, 操作很简单，支持跨平台。

注意：首次使用先备份下本地 hosts.

详细介绍可以阅读 [SwitchHosts! 还能这样管理hosts，后悔没早点用](https://mp.weixin.qq.com/s/A37XnD3HdcGSWUflj6JujQ)

操作步骤

添加一条规则：

方案名: NAS （可以自行命名）

类型：远程

URL 地址：https://gitlab.com/weikeet/hosts/-/raw/main/hosts

自动更新：1个小时（时间可自行调整）

这样就可以和最新的hosts保持同步。



### 手动配置

#### macOS

`hosts`文件位置：`/etc/hosts`。

`macOS`系统下修改需要按照如下方式：

##### 1：首先，打开（访达）Finder

##### 2：使用组合键 `Shift+Command+G` 打开"前往文件夹"，输入框中输入 `/etc/hosts`

##### 3：然后就会跳转到 `hosts` 文件位置

> 注意：如果你使用 `VS Code`，可以直接用 `VS Code` 修改和保存，不需要复制文件。

复制 `hosts` 文件到桌面上，鼠标右键右击它，选择「打开方式」—「文本编辑」，打开这个 `hosts` 文件，把前面的 `hosts` 内容复制进来。

然后把你修改好的 `hosts` 文件替换掉： `/etc/hosts`  文件。

注意：如果弹出密码输入框，你需要输入你当前登录账号对应的密码。

最后刷新缓存：

```shell
sudo killall -HUP mDNSResponder
```

#### Windows

`hosts`文件位置：`C:/windows/system32/drivers/etc/hosts`。

将前文`hosts`内容追加到`hosts`文件，然后刷新`DNS`缓存：

```shell
ipconfig /flushdns
```
